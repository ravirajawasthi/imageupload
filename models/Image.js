var mongoose = require("mongoose");
var passportLocalMongoose = require("passport-local-mongoose");

var imageSchema = new mongoose.Schema({
    originalName: String,
    name: String,
    location: String
});


module.exports = mongoose.model("image", imageSchema);