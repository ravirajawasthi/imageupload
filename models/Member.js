var mongoose = require("mongoose");
var passportLocalMongoose = require("passport-local-mongoose");

var memberSchema = new mongoose.Schema({
    username: String,
    password: String,
    images: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "image"
    }]
});

memberSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model("member", memberSchema);