//DEPENDENCIES

var express = require("express");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var multer = require("multer");
var path = require("path");
var passport = require("passport");
var LocalStrategy = require("passport-local");
var cookieSession = require("cookie-session");


// MONGODB SETUP

mongoose.connect("mongodb://localhost:27017/imgupload", {
    useNewUrlParser: true
});


//Databse Models

var Member = require("./models/Member");
var Image = require("./models/Image");



// MIDDLEWEAR

middlewear = require("./middlewear/middlewear");


// Passport Setup

passport.use(new LocalStrategy(Member.authenticate()));
passport.serializeUser(Member.serializeUser());
passport.deserializeUser(Member.deserializeUser());


// APP COINFIG

var app = express();
app.use(cookieSession({
    name: "session",
    secret: "humpty DumpTy",
    maxAge: 43200000
}));
app.use(bodyParser.urlencoded({ extended: true }))
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static("public"));
app.use(express.static("uploads"));
app.set("view engine", "ejs");
app.use(function(req, res, next) {
    app.locals.user = req.user
    next();
})



// MULTER CONFIGURATION

var storage = multer.diskStorage({
    destination: "uploads",
    filename: (req, file, cb) => {
        cb(null, "Member - " + Date.now() + ".jpeg");
    }
})

var upload = multer({
    limits: 10000000,
    fileFilter: (req, file, callback) => {
        console.log("I am run everytime a file is uploaded");
        checkFileType(req, file, callback);
    },
    storage: storage
}).single("profile");



// FUNCTION TO CHECK FILE TYPE

function checkFileType(req, file, callback) {
    console.log(file.mimetype)
    if (file.mimetype=='image/jpeg' || file.mimetype=='image/png' || file.mimetype=='image/jpg' || file.mimetype=='image/gif') {
        console.log("File will be uploaded");
        callback(null, true);
    } else {
        console.log("File will not be uploaded");
        callback("IMAGES ONLY", false);
    }
}


//ROUTES

app.get("/", (req, res) => {
    res.render("landing");
})

app.get("/login", (req, res) => {
    res.render("login");
})

app.get("/register", (req, res) => {
    res.render("register");
})

app.post("/register", (req, res) => {
    Member.register(new Member({username: req.body.username}), req.body.password, (err, member) => {
        if(err){
            console.log(err);
            res.redirect("back");
        }
        passport.authenticate("local")(req, res, () => {
            res.redirect("/dashboard");
        })
    })
})

app.get("/dashboard", middlewear.isLoggedin, (req, res) => {
    console.log(req.user._id)
    Member.findById({_id: req.user._id}).populate("images").exec(function(err, foundMember){
        res.render("dashboard", {"images": foundMember.images});
    })
})

app.get("/login", (req, res) => {
    res.render("login");
})

app.post("/login", passport.authenticate("local"), (req, res) => {   
    res.redirect("/dashboard");
})

app.post("/upload",middlewearObj.isLoggedin, (req, res) => {
    upload(req, res, (err) => {
        var imageObj = {}
        imageObj.originalName = req.file.originalname
        imageObj.name = req.file.filename
        imageObj.location = req.file.path
        var newImage = new Image(imageObj)
        newImage.save();
        Member.findByIdAndUpdate(req.user.id, {$push: {images: newImage.id}}, (newImage, err) => {
            if (err){
                console.log(err);
                res.redirect("/");
            }
            console.log(newImage);
        });
        res.redirect("/dashboard");
    })
})

app.get("/logout", middlewear.isLoggedin, (req, res) => {
    req.logout();
    res.redirect("/");
})

app.listen(3000, () => {
    console.log("server started at 3000");
})